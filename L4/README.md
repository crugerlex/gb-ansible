# Задание:<br>
1) Модифицировать плейбуки и роли, добавив тэги и добавить использование ansible-vault<br>
2) Сделаем порт nginx конфигурируемым, число worker_count<br>
3) Структурировать плейбуки:<br>
Объединенный плейбук, который подключить остальные плейбуки<br>
плейбук установки nginx<br>
плейбук установки db<br>
плейбук для запуска тестов<br>
4) Написать несколько проверок жизнеспособности всех сервисов<br>
проверить nginx<br>
проверить db<br>
запросить на nginx какой-нибудь статичный файл (uri + get_url)<br>
проверить, что nginx отвечает на запрос < 2s (uri + get_url)<br>
5) Настроить бекапирование db:<br>
написать скрипт бекапирования (python, bash)<br>
создавать crontask из скрипта каждый час (значение сделать конфигурируемым из инвентори)<br>
Бекапы складывать в отдельный каталог<br>
6) Настроить бекапирование nginx по аналогии с 4*<br>

PS pass: password
